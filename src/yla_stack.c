/*
    Int stack implementation

    This file is part of YLA VM (Yet another Language for Academic purpose: Virtual Machine).

    YLA VM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <stdio.h>
#include "yla_stack.h"

static void dprint();

void yla_stack_init(yla_stack* stack, size_t size)
{
    stack->size = size;
    stack->count = 0;
    // allocated to elements to zero
    stack->array = g_array_new(FALSE, TRUE, sizeof(int));
}

void yla_stack_done(yla_stack* stack)
{
    stack->size = 0;
    stack->count = 0;
    g_array_free(stack->array, TRUE);
}

int yla_stack_push(yla_stack* stack, yla_int_type value)
{
    if (stack->count >= stack->size) {
        return 0;
    }
    stack->count++;
    g_array_append_val(stack->array, value);
    return 1;
}

int yla_stack_pull(yla_stack* stack, yla_int_type *result)
{
    if (stack->count == 0) {
        return 0;
    }
    --stack->count;
    *result = g_array_index(stack->array, int, stack->count);
    return 1;
}

int yla_stack_set_deep(yla_stack* stack, size_t index, yla_int_type value)
{
    if (stack->count == 0) {
        return 0;
    }
    if (index >= stack->count) {
        return 0;
    }

    g_array_remove_index(stack->array, stack->count-index-1);
    g_array_insert_val(stack->array,stack->count-index-1, value);
    return 1;
}

int yla_stack_get_deep(yla_stack* stack, size_t index, yla_int_type *result)
{
    if (stack->count == 0) {
        return 0;
    }
    if (index >= stack->count) {
        return 0;
    }

    *result = g_array_index(stack->array, int, stack->count-index-1);
    return 1;
}

int yla_stack_top(yla_stack* stack, yla_int_type *result)
{
    if (stack->count == 0) {
        return 0;
    }

    *result = g_array_index(stack->array, int, stack->count-1);
    return 1;
}

int yla_stack_is_empty(yla_stack* stack)
{
    return stack->count == 0;
}

int yla_stack_is_full(yla_stack* stack)
{
    return stack->count >= stack->size;
}

static void dprint(yla_stack* stack)
{
    int i;
    printf("stack:{size: %zu, count: %zu values: ", stack->size, stack->count);
    for (i=0; i < stack->count; ++i) {
        printf("%d ", g_array_index(stack->array, int, stack->count));
    }
    printf("}\n");
}
